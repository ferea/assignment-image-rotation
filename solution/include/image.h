#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H
#include "bmp.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };
struct image {
    uint64_t width, height;
    struct pixel *data;
};
uint64_t paddingCalc(uint64_t width);
int releaseImage(struct image* img);
struct image makeImage(size_t width, size_t height);
struct image rotate(struct image image);

#endif
