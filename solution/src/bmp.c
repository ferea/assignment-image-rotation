#include "../include/bmp.h"
#include <stdio.h>

#define BH_TYPE 0x4D42
#define BH_SIZE 40
#define BH_BIT_COUNT 24
#define BH_ZERO 0
#define BH_ONE 1

static struct bmp_header newHeader(struct image const *img) {
    uint64_t imageSize = img->width * img->height * sizeof(struct pixel);
    uint64_t paddingSize = img->height * paddingCalc(img->width);
    return (struct bmp_header) {
            .bfType = BH_TYPE,
            .bfileSize = imageSize + paddingSize + sizeof(struct bmp_header),
            .bfReserved = BH_ZERO,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BH_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BH_ONE,
            .biBitCount = BH_BIT_COUNT,
            .biCompression = BH_ZERO,
            .biXPelsPerMeter = BH_ZERO,
            .biYPelsPerMeter = BH_ZERO,
            .biClrUsed = BH_ZERO,
            .biClrImportant = BH_ZERO,
            .biSizeImage = img->height * img->width * sizeof(struct pixel)
    };
}

enum read_status from_bmp(FILE *input, struct image *img) {
    if (!input) {
        return READ_ERROR;
    } else {
        struct bmp_header header;
        if (fread(&header, sizeof(struct bmp_header), 1, input) != 1) {
            return READ_ERROR;
        }        *img = makeImage(header.biWidth, header.biHeight);
        uint64_t padding = paddingCalc(header.biWidth);
        for (size_t row = 0; row < img->height; row++) {
            size_t read = fread(img->data + row * img->width, sizeof(struct pixel), img->width,
                                input);
            if (read != img->width) {
                return READ_ERROR;
            }
            unsigned char paddingData[4];
            if (fread(paddingData, 1, padding, input) != padding) {
                return READ_ERROR;
            }        }
        return READ_OK;
    }
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if (!out) {
        return WRITE_ERROR;
    } else {
        struct bmp_header header = newHeader(img);
        if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
            return WRITE_ERROR;
        }
        for (size_t i = 0; i < img->height; i++) {
            size_t write = fwrite(img->data + img->width * i, sizeof(struct pixel), img->width, out);
            if (write != img->width) {
                return WRITE_ERROR;
            }
            uint64_t padding = paddingCalc(img->width);
            uint64_t padding_data = 0;
            if (fwrite(&padding_data, 1, padding, out) != padding) {
                return WRITE_ERROR;
            }
        }
        return WRITE_OK;
    }
}

enum read_status printReadStatus(enum read_status status) {
    switch (status) {
        case READ_OK:
            printf("Successful reading.\n");
            break;
        case READ_ERROR:
            printf("Read error.\n");
            break;
        default:
            printf("Unknown status.\n");
            break;
    }
    return status;
}

enum write_status printWriteStatus(enum write_status status) {
    switch (status) {
        case WRITE_OK:
            printf("Successful writing.\n");
            break;
        case WRITE_ERROR:
            printf("Write error.\n");
            break;
        default:
            printf("Unknown status.\n");
            break;
    }
    return status;
}

