#include "../include/bmp.h"
#include <stdlib.h>

int releaseImage(struct image *img) {
    if (img->data != NULL) {
        free(img->data);
        img->data = NULL;
    }
    return 0;
}

struct image makeImage(size_t width, size_t height) {
    struct image img;
    img.width = width;
    img.height = height;
    img.data = (struct pixel *) malloc(sizeof(struct pixel) * width * height);
    if (img.data == NULL) {
        fprintf(stderr, "Failed to allocate memory for image data.");
    }
    return img;
}

struct image rotate(struct image image) {
    size_t width = image.height;
    size_t height = image.width;
    struct image newImg = makeImage(width, height);
    if (newImg.data) {
        for (uint64_t row = 0; row < newImg.height; row++) {
            for (uint64_t column = 0; column < newImg.width; column++) {
                size_t srcRow = image.height - 1 - column;
                size_t srcCol = row;
                size_t destRow = row;
                size_t destCol = column;
                if (srcRow < image.height && srcCol < image.width &&
                    destRow < newImg.height && destCol < newImg.width) {
                    struct pixel *srcPixel = &image.data[srcRow * image.width + srcCol];
                    struct pixel *destPixel = &newImg.data[destRow * newImg.width + destCol];
                    *destPixel = *srcPixel;
                }
            }
        }
    }
    return newImg;
}

uint64_t paddingCalc(uint64_t width) {
    uint64_t rowSize = width * sizeof(struct pixel);
    uint64_t padding = 0;
    if (rowSize % 4 != 0) {
        padding = 4 - (rowSize % 4);
    }
    return padding;
}
