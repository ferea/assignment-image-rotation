#include "../include/bmp.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Incorrect number of arguments.");
        return 1;
    }
    const char *input = argv[1];
    const char *output = argv[2];
    struct image img = {0};
    FILE *openRead = fopen(input, "rb");
    if (!openRead) {
        fprintf(stderr, "Error opening input file.");
        return 2;
    }
    enum read_status readBMP = from_bmp(openRead, &img);
    fclose(openRead);
    if (readBMP != READ_OK) {
        printReadStatus(readBMP);
        releaseImage(&img);
        return 3;
    }
    struct image rot = rotate(img);
    FILE *out = fopen(output, "wb");
    if (!out) {
        fprintf(stderr, "Error opening output file.");
        releaseImage(&img);
        releaseImage(&rot);
        return 4;
    }
    enum write_status writeBMP = to_bmp(out, &rot);
    if (writeBMP != WRITE_OK) {
        printWriteStatus(writeBMP);
        releaseImage(&img);
        releaseImage(&rot);
        fclose(out);
        return 5;
    }
    fclose(out);
    printReadStatus(readBMP);
    printWriteStatus(writeBMP);
    releaseImage(&img);
    releaseImage(&rot);
    return 0;
}

